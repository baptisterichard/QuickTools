rem Script to automatically fetch and install update, then reboot as needed
rem if placed in windows boot folder, will then automatically start again after reboot

:CheckForRequiredRestart
	wuauclt /detectnow
	wuauclt /updatenow
    reg query "HKLM\Software\Microsoft\Windows\CurrentVersion\WindowsUpdate\Auto Update\RebootRequired" > nul && shutdown -r -t 0
    ping 127.0.0.1 -n 61 > nul
goto CheckForRequiredRestart